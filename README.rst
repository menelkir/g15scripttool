G15SCRIPTTOOL
=============

G15ScripTool is a simple shellscript helper.
The commands are BASIC-like, and quite simple to read.
Most commands follow the same arguments as libg15render's versions.

========
Commands
========

:g15new:         start a new screen
:g15print:       output text
:g15bar:         draw a percentage bar
:g15line:        draw a line between two points
:g15box:         draw a box, optionally filled with a colour.
:g15circle:      draw a circle, optionally filled with a colour.
:g15clear:       clear screen.
:g15roundbox:    draw a box with rounded corners, optionally filled with a colour.
:g15setpixel:    draw a single pixel in specified colour.
:g15flush:       write the buffer to the LCD
:g15quit:        exit screen

========
Examples
========

A simple example can be found in test.sh in this directory, here is a snippet:

.. code-block:: bash

 #!/bin/sh 
 g15new
 g15print "Percentage Bar",0,1,1,1,i
 for i in `seq 1 10`; do
        g15bar 1,20,160,30,1,$i,10,0
        g15flush
        sleep .2
 done
 g15flush
 sleep 1
 g15quit

====
TODO
====

- Write documentation
- Add truetype font support
- Add ability to receive keypresses
- Add wbmp support for splash screens etc.

=============
How to donate
=============

If you find this repo useful (don't forget to pay a visit to the related
repos too), you can buy me a beer:

:BTC: 3ECzX5UhcFSRv6gBBYLNBc7zGP9UA5Ppmn

:ETH: 0x7E17Ac09Fa7e6F80284a75577B5c1cbaAD566C1c

